create database employeemanagement;
use employeemanagement;

create table Employee(
empId int unsigned primary key,
empName varchar(50),
empEmail varchar(60),
empDoj date not null,
managerId int unsigned,
constraint foreign key (managerid) references Employee(empId)
);
 

insert Employee(empId, empName, empEmail,empDoj)
values
(1000 , "Jagadeesh" , "jagadeesh@gmail.com","2004-01-01"),
(1001 , "Karthik" , "karthik@gmail.com","2018-01-01"),
(1002 , "Akshaya" , "akshaya@gmail.com" ,"2018-01-01"),
(1003 , "Spoorthy" , "spoorthy@gmail.com","2018-01-01"),
(1004 , "Aishwarya" , "asihwaria@gmail.com","2018-01-01"),
(1005, "Diwansh" , "diwansh@gmail.com","2018-01-01");


