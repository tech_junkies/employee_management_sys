package com.hexaware.ems.dao;
import com.hexaware.ems.model.*;

import java.util.Set;

public interface EmployeeDetailsDAO {

	EmployeeDetails save(Long	empId, EmployeeDetails ed);
	EmployeeDetails	findById(long empId);
	void deleteById(long empId);
	Set<EmployeeDetails>	getAllEmployeeDetails(); 
	
	
} 
