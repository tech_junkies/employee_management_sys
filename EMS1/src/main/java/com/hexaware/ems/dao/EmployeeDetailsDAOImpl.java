package com.hexaware.ems.dao;
import java.util.HashSet;
import java.util.Set;
import	com.hexaware.ems.model.*;
import	com.hexaware.ems.utility.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class EmployeeDetailsDAOImpl implements EmployeeDetailsDAO{
	 private final JDBCUtility jdbcUtil;

	   public EmployeeDetailsDAOImpl(JDBCUtility jdbcUtil){
	        this.jdbcUtil = jdbcUtil;
	    }



	@Override
	public EmployeeDetails save(Long empId, EmployeeDetails ed) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeDetails findById(long empId) {
        Connection connection = null;
        try {
            connection = jdbcUtil.getDBConnection("root", "Password123");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from employee where empId=" + empId);
            if (resultSet.next()){
                Long employeeId = resultSet.getLong(1);
                String empName = resultSet.getString(2);
                String email = resultSet.getString(3);
                LocalDate date = resultSet.getDate(4).toLocalDate();
               
                //EmployeeDetails managerId = findById(resultSet.getInt(5));
                EmployeeDetails employee = new EmployeeDetails(employeeId, empName, date, email, null);
                return  employee;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



	@Override
	public void deleteById(long empId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<EmployeeDetails> getAllEmployeeDetails()  {
		Connection	connection=null;
		// TODO Auto-generated method stub
		try {
		connection = JDBCUtility.getDBConnection("root", "Password123");
        Statement statement = connection.createStatement();
        ResultSet	resultSet=statement.executeQuery("select * from employee");
        Set<EmployeeDetails> employeeDetails=new	HashSet<>();
        if(resultSet.next()) {
        	int employeeId = resultSet.getInt(1);
            String empName = resultSet.getString(2);
            String emailString=resultSet.getString(3);
            LocalDate dojDate=resultSet.getDate(4).toLocalDate();
            EmployeeDetails	manager=findById(resultSet.getInt(5));;
            EmployeeDetails employeeDetails2 = new EmployeeDetails(employeeId, empName,dojDate,emailString,manager);
        	employeeDetails.add(employeeDetails2);
        }
        return	employeeDetails;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}
	
	

}
