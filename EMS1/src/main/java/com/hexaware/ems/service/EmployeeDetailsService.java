package com.hexaware.ems.service;

//import java.util.Set;

import com.hexaware.ems.model.EmployeeDetails;

public interface EmployeeDetailsService {
	EmployeeDetails save(Long	empId, EmployeeDetails ed);
	EmployeeDetails	findById(long empId);
	void deleteById(long empId);
}
