package com.hexaware.ems.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtility {

	public static Connection getDBConnection(String username, String password) throws SQLException {
        Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/employeemanagement", username, password);

        return connection;      
    }

}
